<?php
//crear array de alumnos e imprimir su contenido
$alumnos = [
    "alumno1" => "Ramon",
    "alumno2" => "Jose",
    "alumno3" => "Pepe",
    "alumno4" => "Ana",
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>

<body>
    <?php
    //utilizamos foreach para imprimir el array
    foreach ($alumnos as $c) {
        echo "<br>$c";
    }


    ?>
</body>

</html>