<?php
$a = [23, 44, 55, 67, 89];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <?php
    foreach ($a as $v) {
        echo "<br>$v";
    }
    //muestra los valores del array porque los recorre
    ?>
</body>

</html>