<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>
    <?php

    for ($c = 0; $c < 10; $c++) {
        echo "$c <br>";
        echo '$c <br>';
        echo $c . "<br>";
    }
    //la salida es un contador subiendo de 1 en 1 hasta 9
    //El echo '$c <br>' muestra el nombre de la variable literal y no su valor
    ?>

</body>

</html>