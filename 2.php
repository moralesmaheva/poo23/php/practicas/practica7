<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <?php
    $entero = 10;
    $cadena = "hola";
    $real = 23.6;
    $logico = TRUE;

    var_dump($entero);
    var_dump($cadena);
    var_dump($real);
    var_dump($logico);

    $logico = (int)$logico;
    $entero = (float)$entero;
    settype($logico, "int");

    var_dump($entero);
    var_dump($cadena);
    var_dump($real);
    var_dump($logico);
    //se han cambiado el tipo de dato a $logigo a int y $entero a float
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:17:int 10
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:18:string 'hola' (length=4)
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:19:float 23.6
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:20:boolean true
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:26:float 10
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:27:string 'hola' (length=4)
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:28:float 23.6
    // C:\laragon\www\poo2023\php\practicas\practica7\2.php:29:int 1
    ?>
</body>

</html>