<?php
//crear array con 10 numeros aleatorios y mostrar el array en pantalla
$numeros = [];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 10</title>
</head>

<body>
    <?php
    //utilizamos for
    for ($c = 0; $c < 10; $c++) {
        $numeros[$c] = mt_rand(1, 100);
        echo "<br>$numeros[$c]";
    }

    //utilizando foreach
    foreach ($numeros as $value) {
    }
    for ($c = 0; $c < 10; $c++) {
        $value = $numeros[$c] = mt_rand(1, 100);
        echo "$value<br>";
    };
    ?>
</body>

</html>